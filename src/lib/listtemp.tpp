#include "../inc/listtemp.h"
#include <iostream>


template<class T>
void List<T>::push_back(T i)
{
    ListNode * Node = new ListNode;
    Node->_value=i;

    _elem_num++;

    if (_tail!=nullptr)
    {
        _tail->_next=Node;
    }

    _tail=Node;

    if (nullptr==_head)
    {
        _head=Node;
    }

    return;

}

template<class T>
List<T>::~List() {

    if (_elem_num!=0)
    {
        auto iterator = _head;

        while(true)
      {
            auto temp=iterator->_next;

            delete iterator;

            _elem_num--;
            if (0 == _elem_num)
            {
                delete temp;
                break;
            }
            iterator = temp;

     }
    }
}


template<class T>
T List<T>::pop_front()
{


    if (_elem_num>0)
    {
        _elem_num--;
        T element_to_return = _head->_value;
        auto Temp = _head->_next;
        delete _head;
        _head = Temp;
        if (_elem_num==0)

        {   _head=nullptr;
            _tail=nullptr;
        }
        return element_to_return;
    }

    else
    {
       std::cout <<"brak elementow w kolejce"<< "\n";
    }

}

template<class T>
void List<T>::print()
{
    if (_elem_num!=0)
    {
        auto iterator = _head;


        while(true)
        {

            std::cout<< "element" << iterator->_value << '\n';
            auto temp=iterator->_next;


            if (iterator == _tail)
            {
                break;
            }
            iterator = temp;

        }
    }
    else
    {
        std::cout <<"brak elementow w kolejce"<< "\n";
    }
}
