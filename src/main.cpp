// szablon listy


#include <iostream>
#include "inc/calcint.h"
#include "inc/calcdouble.h"
#include "inc/calcstruct.h"
#include "inc/calctemp.h"
#include <bitset>
#include <cstdlib>
#include <cstdio>
#include "inc/listtemp.h"
#include "inc/stacktemp.h"
#include <list>
#include "test.h"
#include "tempautoptr.h"

Point operator*(const Point& a,const Point& b);
Point operator+(const Point& a,const Point& b);

int test5(const double &x)
{
    return x;
};

template <typename T>
T multiply( T arg)
{

    return arg;

}


template <typename T, typename... args>
T multiply ( T first, args... T2)
{

    return first * multiply(T2...);


}

template <typename T>
T addvariadic (T arg)
{
return arg;
}


template <typename T, typename... args>
T addvariadic (T first, args... T2)
{

    return first + addvariadic(T2...);

}

//template <typename T>


using namespace std;



ostream & operator<< (ostream & wyjscie,const Point& b)
{
    cout << __PRETTY_FUNCTION__ << '\n';
    wyjscie << "Wspolrzedne a: " << b.xxx << '\n';
    wyjscie << "Wspolrzedne b: " << b.yyy << '\n';
    wyjscie << "Odleglosc wektora od środka: " << b.dist_to_center << '\n';

    return wyjscie;
}







using namespace std;

int main()
{

calcint NUMBER1INT(5,8);
cout << "LICZBY INT" << '\n';
cout << "DODOWANIE" << '\n';
cout << NUMBER1INT.add() << '\n';
cout << "ODEJMOWANIE" << '\n';
cout << NUMBER1INT.min() << '\n';
cout << "MNOZENIE" << '\n';
cout << NUMBER1INT.mul() << '\n';
cout << "DZIELENIE" << '\n';
cout << NUMBER1INT.div() << '\n';


calcdouble NUMBER1DOUBLE(5.3,6.3);
cout << "LICZBY DOUBLE" << '\n';
cout << "DODOWANIE" << '\n';
cout << NUMBER1DOUBLE.add() << '\n';
cout << "ODEJMOWANIE" << '\n';
cout << NUMBER1DOUBLE.min() << '\n';
cout << "MNOZENIE" << '\n';
cout << NUMBER1DOUBLE.mul() << '\n';
cout << "DZIELENIE" << '\n';
cout << NUMBER1DOUBLE.div() << '\n';

Point POINT1{3,2};
Point POINT2{5,2};

calcpoint PointCalc{POINT1, POINT2};
cout << "STRUKTURY" << '\n';
cout << "DODOWANIE" << '\n';
cout << PointCalc.add() << '\n';
cout << "ODEJMOWANIE" << '\n';
cout << PointCalc.min() << '\n';
cout << "MNOZENIE" << '\n';
cout << PointCalc.mul() << '\n';
cout << "DZIELENIE" << '\n';
cout << PointCalc.div() << '\n';


calctemp<int> temp(3,3);
cout << "SZABLON" << '\n';
cout << "DODOWANIE" << '\n';
cout << temp.add() << '\n';
cout << "ODEJMOWANIE" << '\n';
cout << temp.min() << '\n';
cout << "MNOZENIE" << '\n';
cout << temp.mul() << '\n';
cout << "DZIELENIE" << '\n';
cout << temp.div() << '\n';

calctemp<std::string> temp2("a","a");

cout << "**********************************" << '\n';
cout << "PUNKT 1 " << '\n'<< POINT1 << '\n';
cout << "PUNKT 2 " << '\n'<< POINT2 << '\n';
cout << "**********************************" << '\n';


cout << "std string " << temp2.add()<< '\n';
cout << "SZABLON FUNKCJI WARIADYCZNY: " << '\n'<< "mnożenie: " << multiply(POINT1, POINT2)<< '\n';
cout << "dodawanie: " << addvariadic(POINT1,POINT2)<< '\n';

//std::bitset<50> bitset1(199);
//std::bitset<50> bitset2(250);

//cout << bitset1 << '\n';
//cout << bitset2 << '\n';

//std::bitset<50> bitset3 {bitset1 & bitset2};
//cout << "Wynik & " << '\n' << bitset3 << '\n';

auto test = addvariadic(3,4);
auto test2{move(test)};
cout <<  '\n' << "test " <<  test;
cout <<  '\n' << "test " <<  test2<< '\n';

Point POINTUUU(3,2);  // KONSTRUKTOR DOMYSLNY
Point POINTXXX(4,4);  // KONTRUKTOR DOMYSLNY
Point xxx{POINTUUU};  // KONTRUKTOR KOPIUJACY
Point yyy{std::move(POINTUUU)}; // KONSTRUKTOR PRZENOSZACY
POINTUUU=std::move(POINTXXX); // OPERATOR PRZENOSZACY
POINTXXX=POINTUUU; // OPERATOR KOPIUJACY
POINTUUU=std::move(xxx); //OPERATOR PRZENOSZACY

cout << POINTUUU;
cout << xxx;

List <int> mylist;

mylist.push_back(30);
mylist.push_back(20);
mylist.push_back(10);

mylist.print();

cout << "POPING 1ST ELEMENT" << mylist.pop_front() << '\n';

mylist.push_back(40);

cout << "POPING 2ND ELEMENT" << mylist.pop_front() << '\n';

mylist.print();


List <Point> ListOfPoint;
ListOfPoint.pop_front();
ListOfPoint.push_back(POINT1);
ListOfPoint.push_back(POINT1);
ListOfPoint.push_back(POINTUUU);
ListOfPoint.push_back(POINT1);
ListOfPoint.pop_front();
ListOfPoint.print();

cout<< "************************************" << '\n';
Stack<int> StackOfInt;
StackOfInt.push(3);
StackOfInt.push(4);
StackOfInt.push(5);
cout << StackOfInt.pop()<<'\n' ;
cout << StackOfInt.pop()<<'\n' ;
cout << StackOfInt.pop()<<'\n' ;

int a=2;
a=StackOfInt.pop() ;
int b=StackOfInt.pop() ;

cout << a << "<<---------------->>" << b <<'\n';
StackOfInt.push(4);
StackOfInt.push(5);
StackOfInt.push(5);

cout<< "**********************ssssssssssssssssss**************" << '\n';

int * testowanie = new int;
std::unique_ptr<testowaklasa>testowyunique;
testowyunique=std::make_unique<testowaklasa>(testowanie,new resource);
std::shared_ptr<testowaklasa>testotwyshared2 = std::make_shared<testowaklasa>(testowanie,new resource);
testowaklasa testowyobiekt(testowanie, new resource);
testowyobiekt.foo();
tempautoptr<int>szablonptr(new int);
szablonptr.Foo2();
std::cout<< "************************************" << '\n';
tempautoptr<int>sablonptr2{szablonptr};
std::cout<< "************************************" << '\n';
szablonptr=sablonptr2;
auto zzz = tempautoptr<int>(new int);



return 0;

}
