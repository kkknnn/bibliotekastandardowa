#ifndef CALCTEMP_H
#define CALCTEMP_H


template <class T>
class calctemp
{
public:
    calctemp(T a, T b):member1{a},member2{b}
    {
    }

    T add();
    T min();
    T mul();
    T div();


private:
    T member1;
    T member2;
};


#include "../lib/calctemp.tpp"

#endif // CALCTEMP_H
