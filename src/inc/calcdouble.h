#ifndef CALCDOUBLE_H
#define CALCDOUBLE_H


class calcdouble
{
public:

    calcdouble(double, double);

    double add();
    double min();
    double mul();
    double div();

private:
    double member1;
    double member2;

};



#endif // CALCDOUBLE_H
