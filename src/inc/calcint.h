#ifndef CALCINT_H
#define CALCINT_H


class calcint
{
public:
    calcint(int, int );

    int add();
    int min();
    int mul();
    int div();

private:
    int member1;
    int member2;

};

#endif // CALCINT_H
