#ifndef LISTTEMP_H
#define LISTTEMP_H

#include <iostream>
#include <memory>


template <class T>
class List
{
    public:
    List()=default;
    void push_back(T i);
    T pop_front();
    void print();
    ~List();
private:
    struct ListNode
    {
            T _value;
            struct ListNode* _next = nullptr;
    };

    ListNode* _head = nullptr;
    ListNode* _tail = nullptr;
    int _elem_num = 0;
};


#include "../lib/listtemp.tpp"

#endif // LISTTEMP_H





