﻿#ifndef CALCSTRUCT_H
#define CALCSTRUCT_H
#include <cmath>
#include <iostream>
#include <cstdlib>

struct Point
{
    Point()=default;
    Point(int pointx, int pointy)
    {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        xxx=pointx;
        yyy=pointy;
        dist_to_center=std::sqrt((xxx*xxx+yyy*yyy));
    }

    ~Point()
    {
     std::cout << __PRETTY_FUNCTION__ << '\n';
    }
    Point(const Point &a)
    {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        xxx=a.xxx;
        yyy=a.yyy;
        dist_to_center=a.dist_to_center;
    }
    Point(Point && a)
    {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        std::swap(xxx,a.xxx);
        std::swap(yyy,a.yyy);
        std::swap(dist_to_center,a.dist_to_center);
        a.dist_to_center=0;
        a.xxx=0;
        a.yyy=0;

    }
    Point & operator=(Point & a)
    {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        xxx=a.xxx;
        yyy=a.yyy;
        dist_to_center=a.dist_to_center;
        return *this;
    }
    Point & operator=(Point && a)
    {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        xxx=a.xxx;
        yyy=a.yyy;
        dist_to_center=a.dist_to_center;
        a.dist_to_center=0;
        a.xxx=0;
        a.yyy=0;
        return *this;
    }

    int xxx{};
    int yyy{};
    double dist_to_center{};
};

class calcpoint
{
public:
    calcpoint(Point a , Point b);
    calcpoint(const calcpoint &)=delete;
    calcpoint & operator=(const calcpoint&)=delete;
    calcpoint(calcpoint&&)=delete;
    calcpoint& operator=(calcpoint&&)=delete ;
    ~calcpoint()=default;

    Point add();
    Point min();
    Point mul();
    Point div();
private:
    struct Point member1;
    struct Point member2;


};

#endif // CALCSTRUCT_H
/*
class calcpoint
{
public:

    Point add()
    {
        return member1+member2;

    }
    Point min()
    {
        return member1-member2;
    }
    Point mul()
    {
        return member1*member2;
    }
    Point div()
    {
        return member1/member2;
    }

private:
   struct Point member1;
   struct Point member2;

};
*/
