#ifndef TEMPAUTOPTR_H
#define TEMPAUTOPTR_H
#include <iostream>



template<class T>
class tempautoptr
{
public:
    tempautoptr(T*);
    ~tempautoptr();
    tempautoptr(const tempautoptr& );
    tempautoptr & operator=(const tempautoptr& innypointer)
    {
        if (this==&innypointer)
        {
            return *this;
        }
        else
        {
        delete auto_ptr;
        auto_ptr=new T;
        *auto_ptr=*(innypointer.auto_ptr);
        std::cout << "operator przypisania kopiujący" << '\n';
        return *this;
        }
    }

    void Foo2();
    T* operator->() const
{
    return auto_ptr;
}
private:
    T* auto_ptr{};
};


template<class T>
tempautoptr<T>::tempautoptr(T *autoptr)
{
auto_ptr=autoptr;
}


template <class T>
tempautoptr<T>::tempautoptr(const tempautoptr& a)
{


        auto_ptr= new T;
        *auto_ptr=*a.auto_ptr;
        std::cout << " kontruktor kopiujacy " << '\n';

}



template<class T>
tempautoptr<T>::~tempautoptr()
{

    delete auto_ptr;
}

template <class T>
void tempautoptr<T>::Foo2()
{
   std:: cout << "Twoja funkcja" << '\n';
}

#endif // TEMPAUTOPTR_H
