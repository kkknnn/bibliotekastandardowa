#include <iostream>
#include "calcint.h"
#include "calcdouble.h"
using namespace std;
struct Point
{
    int x;
    int y;
};

template<class T>
class calctemp
{
public:

    calctemp(T, T);

    T add();
    T min();
    T mul();
    T div();

private:
    T member1;
    T member2;

};




Point operator+(Point a, Point b)
{
     Point TEMP {a.x+b.x,a.y+b.y};
     return TEMP;


}


Point operator-(Point a, Point b)
{
     Point TEMP {a.x-b.x,a.y-b.y};
     return TEMP;


}


Point operator*(Point a, Point b)
{
     Point TEMP {(a.x*b.x), (a.y*b.y)};
     return TEMP;


}

Point operator/(Point a, Point b)
{
     Point TEMP {a.x/b.x,a.y/b.y};
     return TEMP;


}

ostream & operator<< (ostream & wyjscie, Point b)
{
    wyjscie << "Wspolrzedne a: " << b.x << '\n';
    wyjscie << "Wspolrzedne b: " << b.y << '\n';
    return wyjscie;
}




class calcpoint
{
public:

    calcpoint ( Point a , Point b): member1{a},member2{b}
    {

    }


    Point add()
    {
        return member1+member2;

    }
    Point min()
    {
        return member1-member2;
    }
    Point mul()
    {
        return member1*member2;
    }
    Point div()
    {
        return member1/member2;
    }

private:
   struct Point member1;
   struct Point member2;

};





using namespace std;

int main()
{

calcint NUMBER1INT(5,8);
cout << "LICZBY INT" << '\n';
cout << "DODOWANIE" << '\n';
cout << NUMBER1INT.add() << '\n';
cout << "ODEJMOWANIE" << '\n';
cout << NUMBER1INT.min() << '\n';
cout << "MNOZENIE" << '\n';
cout << NUMBER1INT.mul() << '\n';
cout << "DZIELENIE" << '\n';
cout << NUMBER1INT.div() << '\n';


calcdouble NUMBER1DOUBLE(5.3,6.3);
cout << "LICZBY DOUBLE" << '\n';
cout << "DODOWANIE" << '\n';
cout << NUMBER1DOUBLE.add() << '\n';
cout << "ODEJMOWANIE" << '\n';
cout << NUMBER1DOUBLE.min() << '\n';
cout << "MNOZENIE" << '\n';
cout << NUMBER1DOUBLE.mul() << '\n';
cout << "DZIELENIE" << '\n';
cout << NUMBER1DOUBLE.div() << '\n';

Point POINT1{3,2};
Point POINT2{5,2};

calcpoint PointCalc{POINT1, POINT2};
cout << "STRUKTURY" << '\n';
cout << "DODOWANIE" << '\n';
cout << PointCalc.add() << '\n';
cout << "ODEJMOWANIE" << '\n';
cout << PointCalc.min() << '\n';
cout << "MNOZENIE" << '\n';
cout << PointCalc.mul() << '\n';
cout << "DZIELENIE" << '\n';
cout << PointCalc.div() << '\n';

return 0;


}



