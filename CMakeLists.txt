cmake_minimum_required(VERSION 3.5)

project(bibliotekastandardowa2)

file (GLOB SOURCES "src/*.cpp"
                   "src/*.h"
                   "src/inc/*.h"
                   "src/lib/*.cpp"
                   "src/lib/*.tpp"
                   "./*.tpp"
                   "./*.h"
      )

add_executable(${PROJECT_NAME} ${SOURCES})

